let bonusesInfo = document.querySelector('.bonuses__info')
let bonusesBtn = document.querySelector('.bonuses__more .show-all')

let charactInfo = document.querySelector('.characteristics__info')
let charactBtn = document.querySelector('.characteristics__more .show-all')

let slotsInfo = document.querySelector('.slots__info')
let slotsBtn = document.querySelector('.slots__more .show-all')

bonusesBtn.addEventListener('click', () => {
    bonusesInfo.classList.toggle('toggle-list');
    bonusesBtn.classList.toggle('toggle-arrow');
});

charactBtn.addEventListener('click', () => {
    charactInfo.classList.toggle('toggle-list');
    charactBtn.classList.toggle('toggle-arrow');
});

slotsBtn.addEventListener('click', () => {
    slotsInfo.classList.toggle('toggle-list');
    slotsBtn.classList.toggle('toggle-arrow');
});                                  

// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}